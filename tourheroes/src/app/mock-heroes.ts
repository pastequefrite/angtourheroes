import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 1, name: 'Iron-Man' },
  { id: 2, name: 'Spider-Man' },
  { id: 3, name: 'Dr Strange' },
  { id: 4, name: 'Black Widow' },
  { id: 5, name: 'Starlord' },
  { id: 6, name: 'Groot' },
  { id: 7, name: 'Thor' },
  { id: 8, name: 'Ant-Man' },
  { id: 9, name: 'Kick-Ass' }
];
